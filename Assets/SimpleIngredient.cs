﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleIngredient : Ingredient
{
    public SimpleIngredient(string name, float weight, float calories, float cost)
    {
        _name = name;
        _weight = weight;
        _calories = calories;
        _cost = cost;
    }

    public override void Boil()
    {
        Debug.Log("Варим " + _name);
    }

    public override void Cut()
    {
        Debug.Log("Режем " + _name);
    }

    public override void Fry()
    {
        Debug.Log("Жарим " + _name);
    }

    public override float GetTotalCalories()
    {
        return _calories * _weight;
    }

    public override float GetTotalCost()
    {
        return _weight * _cost;
    }

    public override float GetTotalWeight()
    {
        return _weight;
    }

    public override void AddIngredient(Ingredient i)
    {
        throw new System.NotImplementedException();
    }

    public override void RemoveIngredient(Ingredient i)
    {
        throw new System.NotImplementedException();
    }
}
