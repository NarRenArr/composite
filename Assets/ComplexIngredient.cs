﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComplexIngredient : Ingredient
{
    public ComplexIngredient(string name, List<Ingredient> ingredients)
    {
        _name = name;
        _ingredients = ingredients;
    }

    private List<Ingredient> _ingredients = new List<Ingredient>();

    public override void Boil()
    {
        throw new System.NotImplementedException();
    }

    public override void Cut()
    {
        throw new System.NotImplementedException();
    }

    public override void Fry()
    {
        throw new System.NotImplementedException();
    }

    public override float GetTotalCalories()
    {
        float totalCalories = 0;

        foreach (Ingredient i in _ingredients)
        {
            totalCalories += i.GetTotalCalories();
        }

        return totalCalories;
    }

    public override float GetTotalCost()
    {
        float totalCost = 0;

        foreach (Ingredient i in _ingredients)
        {
            totalCost += i.GetTotalCost();
        }

        return totalCost;
    }

    public override float GetTotalWeight()
    {
        float totalWeight = 0;

        foreach (Ingredient i in _ingredients)
        {
            totalWeight += i.GetTotalWeight();
        }

        return totalWeight;
    }

    public override void AddIngredient(Ingredient i)
    {
        _ingredients.Add(i);
    }

    public override void RemoveIngredient(Ingredient i)
    {
        _ingredients.Remove(i);
    }
}
