﻿using UnityEngine;

public abstract class Ingredient : MonoBehaviour
{
    protected string _name;
    protected float _weight;
    protected float _calories;
    protected float _cost;

    /* А зОчем здесь конструктор по заданию нужен? Он мешает удобно реализовать ComplexIngredient.
    public Ingredient(string name, float weight, float calorie, float cost)
    {
        _name = name;
        _weight = weight;
        _calories = calorie;
        _cost = cost;
    }
    */

    public abstract void Cut();

    public abstract void Boil();

    public abstract void Fry();

    public abstract float GetTotalCost();

    public abstract float GetTotalWeight();

    public abstract float GetTotalCalories();

    public abstract void AddIngredient(Ingredient i);

    public abstract void RemoveIngredient(Ingredient i);
}
