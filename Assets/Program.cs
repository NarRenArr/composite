﻿using System.Collections.Generic;
using UnityEngine;

public class Program : MonoBehaviour
{
    void Start()
    {
        SimpleIngredient bottomBread = new SimpleIngredient("Хлеб", 60, 180, 18);
        SimpleIngredient ketchup = new SimpleIngredient("Кепчук", 20, 50, 10);
        SimpleIngredient mayonnaise = new SimpleIngredient("Мазик", 15, 70, 9);
        SimpleIngredient cheese = new SimpleIngredient("Сыр", 50, 100, 150);
        SimpleIngredient sausage = new SimpleIngredient("Колбаса", 100, 150, 70);
        SimpleIngredient topBread = new SimpleIngredient("Хлеб", 40, 120, 12);

        ComplexIngredient cetchinez = new ComplexIngredient("Кетчинез", new List<Ingredient>());
        cetchinez.AddIngredient(ketchup);
        cetchinez.AddIngredient(mayonnaise);

        ComplexIngredient sandwich = new ComplexIngredient("Бутер", new List<Ingredient>());
        sandwich.AddIngredient(bottomBread);
        sandwich.AddIngredient(cetchinez);
        sandwich.AddIngredient(cheese);
        sandwich.AddIngredient(sausage);
        sandwich.AddIngredient(topBread);

        Debug.Log("Вес " + sandwich.GetTotalWeight());
        Debug.Log("Цена " + sandwich.GetTotalCost());
        Debug.Log("Калории " + sandwich.GetTotalCalories());
    }
}
